package com.entity;

import javax.annotation.Generated;
import javax.persistence.metamodel.SingularAttribute;
import javax.persistence.metamodel.StaticMetamodel;

@Generated(value="EclipseLink-2.5.2.v20140319-rNA", date="2017-02-22T02:07:34")
@StaticMetamodel(Products.class)
public class Products_ { 

    public static volatile SingularAttribute<Products, Integer> unitPrice;
    public static volatile SingularAttribute<Products, Integer> unitsInStock;
    public static volatile SingularAttribute<Products, Integer> reorderLevel;
    public static volatile SingularAttribute<Products, Integer> supplierID;
    public static volatile SingularAttribute<Products, Integer> productID;
    public static volatile SingularAttribute<Products, String> quantityPerUnit;
    public static volatile SingularAttribute<Products, Boolean> discontinued;
    public static volatile SingularAttribute<Products, String> productName;
    public static volatile SingularAttribute<Products, Integer> categoryID;
    public static volatile SingularAttribute<Products, Integer> unitsOnOrder;

}