/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.ws;

import com.controller.ProductsJpaController;
import com.entity.Products;
import com.entity.Result;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;
import javax.ws.rs.ApplicationPath;
import javax.ws.rs.DELETE;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.Application;
import javax.ws.rs.core.MediaType;

/**
 *
 * @author codeid
 */
@Path("/products")
@ApplicationPath("/api")
public class ProductsWs extends Application{ 
    EntityManagerFactory emf = Persistence.createEntityManagerFactory("InventoryPU");  
    ProductsJpaController controller = new ProductsJpaController(emf);
    @GET
    @Path("/hello")
    public String GetHello(){ 
        return "<h1>Hello JAX</h1>";
    }
    
    @GET
    @Path("/find/{id}")
    @Produces(MediaType.APPLICATION_JSON)
    public Products getProductByID(@PathParam("id") int id){ 
        Products p =  controller.findProducts(id);
        return p;
    }
    
    @GET
    @Path("/page/{first}/{max}")
    @Produces(MediaType.APPLICATION_JSON)
    public List<Products> getProductPage(@PathParam("first") int first,@PathParam("max") int max){ 
        List<Products> list =  controller.findProductsEntities(max, first);
        return list;
    }
    
    @GET
    @Path("/findall")
    @Produces(MediaType.APPLICATION_JSON)
    public List<Products> getAllProduct(){ 
        List<Products> list =  controller.findProductsEntities();
        return list;
    }
    
    @POST
    @Path("/insert")
    @Produces(MediaType.APPLICATION_JSON)
    public Result insert(Products p){ 
        Result result = null;
        try { 
            controller.create(p);
            result = new Result(200,"ok","Insert data success!");
        } catch (Exception ex) {
            Logger.getLogger(ProductsWs.class.getName()).log(Level.SEVERE, null, ex);
            result = new Result(200,"error","Insert data fail!");            
        } 
        return result;
    }
    
    @PUT
    @Path("/update")
    @Produces(MediaType.APPLICATION_JSON)
    public Result update(Products p){
        Result result = null;
        try {
            controller.edit(p);
            result = new Result(200,"ok","Update data success!");
        } catch (Exception ex) {
            Logger.getLogger(ProductsWs.class.getName()).log(Level.SEVERE, null, ex);
            result = new Result(200,"error","Update data fail!");
        } 
        return result;
    }
    
    @DELETE
    @Path("/delete/{id}")
    @Produces(MediaType.APPLICATION_JSON)
    public Result update(@PathParam("id") int id){
        Result result = null;
        try {
            controller.destroy(id);
            result = new Result(200,"ok","Delete data success!");
        } catch (Exception ex) {
            Logger.getLogger(ProductsWs.class.getName()).log(Level.SEVERE, null, ex);
            result = new Result(200,"error","Delete data fail!");
        } 
        return result;
    }
}
