/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.entity;

import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author codeid
 */
@XmlRootElement
public class Result {
    private int code;
    private String status;
    private String message;

    public Result() {
    }

    public Result(int code, String status, String message) {
        this.code = code;
        this.status = status;
        this.message = message;
    }      

    public int getCode() {
        return code;
    }

    public void setCode(int code) {
        this.code = code;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }
    
    
}
